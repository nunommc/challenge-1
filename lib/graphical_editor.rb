class GraphicalEditor
  attr_accessor :image

  def initialize bootstrap_image=nil
    @image = bootstrap_image || []
  end

  def process_input commands
    commands.each do |command|
      run_command(command)
    end
    nil
  end

  def run_command command
    case command
    when /^I (\d+) (\d+)/
      create_image $1.to_i, $2.to_i

    when /^L (\d+) (\d+) (\w)/
      x = $1.to_i - 1 ; y = $2.to_i - 1 ; colour = $3
      @image[y][x] = colour

    when /^C/
      @image.each{|line| line.fill 'O'}

    when /^S/
      @image && @image.each{|line| puts line.join ''} ; puts

    when /^V (\d+) (\d+) (\d+) (\w)/
      x = $1.to_i - 1 ; y1 = $2.to_i - 1 ; y2 = $3.to_i - 1 ; colour = $4
      draw_vertical x, y1, y2, colour
      
    when /^H (\d+) (\d+) (\d+) (\w)/
      x1 = $1.to_i - 1 ; x2 = $2.to_i - 1 ; y = $3.to_i - 1 ; colour = $4
      draw_horizontal x1, x2, y, colour
   
    when /^F (\d+) (\d+) (\w)/
      x = $1.to_i - 1 ; y = $2.to_i - 1 ; colour = $3
      fill_region(x, y, colour)
   
    when /^X/
      exit(0)
    else
      puts "#{command} not supported (yet?)"
    end
  end

  private #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
    def create_image m, n
      @image.clear
      n.times { @image << Array.new(m, 'O') }
    end

    def fill_region x, y, colour
      current_colour = @image[y][x]
      # puts "Current Colour is #{current_colour}"

      fill_sideways(x, y, current_colour, colour)
    end

    def fill_sideways x, y, original_colour, colour, direction=nil
      return if x < 0 || y < 0 || y >= @image.size || x >= @image[y].size || @image[y][x] != original_colour

      @image[y][x] = colour
      fill_sideways(x,  y+1, original_colour, colour, :down)   unless direction == :up
      fill_sideways(x,  y-1, original_colour, colour, :up)     unless direction == :down
      fill_sideways(x+1,  y, original_colour, colour, :right)  unless direction == :left
      fill_sideways(x-1,  y, original_colour, colour, :left)   unless direction == :right
    end

    def draw_vertical x, y1, y2, colour
      !@image.empty? && (y1..y2).each{|y| @image[y][x] = colour}
    end

    def draw_horizontal x1, x2, y, colour
      !@image.empty? && @image[y].fill(colour, x1..x2)
    end

end
