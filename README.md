# README #

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

* How to run tests
```
#!shell

$ rspec spec/lib/graphical_editor_spec.rb -fdoc
```
or, to run it with the given input
```
#!shell

$ ruby bin/run_fill_image.rb -a
```

or, to run it interactively
```
#!shell

$ ruby bin/run_fill_image.rb
```