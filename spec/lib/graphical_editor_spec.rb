require './lib/graphical_editor'
require 'pp'

describe GraphicalEditor do
  let(:editor) { GraphicalEditor.new }

  context '#run_command' do
    describe 'I N M' do
      it 'creates an image NxM' do
        expect { editor.send :create_image, 3, 2 }.
          to change { editor.image }.
          to [["O", "O", "O"], ["O", "O", "O"]]
      end
    end

    context 'has an image 3x2' do
      before { editor.send :create_image, 3, 2 }

      describe 'H X1 X2 Y C' do

        it 'draws one h-segment of colour C' do
          expect {
            editor.send(:draw_horizontal, 0, 1, 0, 'C')
          }.to change { editor.image.to_s }.
          from([["O", "O", "O"], ["O", "O", "O"]].to_s).
          to   [["C", "C", "O"], ["O", "O", "O"]].to_s

        end
      end
    end

  end
end
