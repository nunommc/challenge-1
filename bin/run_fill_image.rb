require './lib/graphical_editor'

editor = GraphicalEditor.new

begin
  # automatic
  if ARGV[0] == '-a'
    commands = ['I 5 6', 'L 2 3 A', 'S', 'F 3 3 J', 'V 2 3 4 W', 'H 3 4 2 Z', 'S']
    editor.process_input commands

  # interactive
  else
    loop { editor.run_command gets.chomp }
  end
end